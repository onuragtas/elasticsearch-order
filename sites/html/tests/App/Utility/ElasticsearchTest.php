<?php


namespace Tests\App\Utility;


use PHPUnit\Framework\TestCase;
use service\Utility\Elasticsearch;

define('ELASTIC_BASE_URL', 'http://172.28.1.5:9200');

class ElasticsearchTest extends TestCase
{

    public function testCreateIndex()
    {
        $this->elastic = new Elasticsearch();
        $this->elastic->index = 'test';
        $response = $this->elastic->create_index();
        $this->assertNotEquals(null, $response);
    }

    public function testIndex()
    {
        $this->elastic = new Elasticsearch();
        $this->elastic->index = 'test';
        $response = $this->elastic->index(['key' => 'value']);
        $this->assertNotEquals(null, $response['_id']);
    }

    public function testFilter()
    {
        $this->elastic = new Elasticsearch();
        $this->elastic->index = 'test';
        $filter = $this->elastic->filter(['must' => ['key' => 'value']]);
        $response = $this->elastic->get($filter['hits']['hits'][0]['_id']);
        $this->assertNotEquals(null, $response['_id']);
    }

    public function testUpdate()
    {
        $this->elastic = new Elasticsearch();
        $this->elastic->index = 'test';
        $filter = $this->elastic->filter(['must' => ['key' => 'value']]);
        $getData = $this->elastic->get($filter['hits']['hits'][0]['_id']);
        $source = $getData['_source'];
        $id = $filter['hits']['hits'][0]['_id'];
        $response = $this->elastic->update($id, $source);
        $this->assertNotEquals(null, $response['_id']);
    }

    public function testDelete()
    {
        $this->elastic = new Elasticsearch();
        $this->elastic->index = 'test';
        $filter = $this->elastic->filter(['must' => ['key' => 'value']]);
        $getData = $this->elastic->get($filter['hits']['hits'][0]['_id']);
        $source = $getData['_source'];
        $id = $filter['hits']['hits'][0]['_id'];
        $response = $this->elastic->delete($id);
        $this->assertNotEquals(null, $response['_id']);
    }

}