<?php


namespace Tests\App\Controller;


class IndexTest extends TestController
{

    public function testRunApp(){
        $response = $this->app('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testIndex(){
        $response = $this->app('GET', '/index');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSearch(){
        $response = $this->app('POST', '/search', ['term'=>'test']);
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testDetail(){
        $response = $this->app('GET', '/detail?id=8YxSuWwBe2-oD9seD61-&term=test');
        $this->assertEquals(200, $response->getStatusCode());
    }


}