<?php


namespace Tests\App\Controller;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;


class TestController extends TestCase
{

    public function app($method, $uri, $data = null){
        $client = new Client(['base_uri' => 'http://localhost/']);
        if($data) {
            return $client->request($method, $uri, ['form_params'=>$data]);
        }else{
            return $client->request($method, $uri);
        }
    }


    public function fw($message){
        fwrite(STDERR, print_r($message, TRUE));
    }
}