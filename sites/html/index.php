<?php

function pp($val, $isDie = 0, $isVarDump = 0)
{
    if ($isVarDump != 0) {
        var_dump($val);
    } else {
        echo '<pre>';
        print_r($val);
        echo '</pre>';
    }
    if ($isDie != 0) {
        die;
    }
}

function autoload($className)
{
    $classPath = __DIR__.'/'.str_replace('\\', '/', $className).'.php';
    if (is_readable($classPath)) {
        require($classPath);
    }
}

$GLOBAL_ENV = parse_ini_file(__DIR__.'/.env');
include 'vendor/autoload.php';
spl_autoload_register('autoload');

// Application Config
define('ROOT_DIR', __DIR__);
define('APP_DIR', ROOT_DIR.'/App');
define('CORE_DIR', APP_DIR.'/Core');
define('MDIR', APP_DIR.'/Model');
define('VDIR', APP_DIR.'/View');
define('CDIR', APP_DIR.'/Controller');
define('UDIR', APP_DIR.'/Utility');
define("PUBLIC_ROOT", "/");

//Assest Conf
define(
    "APP_PROTOCOL",
    (!empty($_SERVER['HTTPS']) &&
        $_SERVER['HTTPS'] !== 'off' ||
        $_SERVER['SERVER_PORT'] == 443) ?
        "https://" : "http://"
);

//App Protocol
define('APP_URL', APP_PROTOCOL . $_SERVER["HTTP_HOST"]);

// Default Controller Config
define('CONTROLLER_PATH', '\App\Controller\\');
define("INDEX_CONTROLLER", CONTROLLER_PATH . "index");
define('INDEX_ACTION', 'main');

// elasticsearch config

define('ELASTIC_BASE_URL', $GLOBAL_ENV['ELASTIC_BASE_URL']);

//$App = new App\Core\App;
//$App->run();

$app = new \Slim\App();
require __DIR__ . '/config/router.php';
$app->run();