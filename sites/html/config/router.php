<?php

$app->get('/', \App\Controller\Index::class.':index');
$app->get('/index', \App\Controller\Index::class.':index');
$app->post('/search', \App\Controller\Index::class.':search');
$app->get('/detail', \App\Controller\Index::class.':detail');