<?php
namespace App\Utility;

class Input
{
    public static function get($key, $default = "")
    {
        return(isset($_GET[$key]) ? $_GET[$key] : $default);
    }

    public static function post($key){
        return $_POST[$key];
    }
}