<?php


namespace service\Utility;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;


class Elasticsearch
{

    public $document = "default";
    public $index = "products";
    public $type = '_doc';
    public $headers = ['Content-Type: application/json'];
    public $default_url;
    public $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()->setHosts([ELASTIC_BASE_URL])->build();
    }

    public function create_index(){
        $params = [
            'index' => $this->index
        ];

        try {
            return $this->client->indices()->create($params);
        }catch (BadRequest400Exception $e){
            return $e->getMessage();
        }
    }

    public function index($params){
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $params
        ];

        return $this->client->index($params);
    }

    public function get($id){
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id
        ];

        return $this->client->get($params);
    }

    public function update($id, $params){
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id,
            'body' => ['doc' => $params]
        ];

        return $this->client->update($params);
    }

    public function delete($id)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id
        ];

        return $this->client->delete($params);
    }

    public function filter($term, $sortby = null, $sort = null)
    {
        try {
            $params = [
                'index' => $this->index,
                'type' => $this->type,
                "from" => 0,
                "size" => 1000,
                'body' => [
                    'query' => [
                        'bool' => [
                            'must' => [],

                        ]
                    ]
                ]
            ];

            foreach ($term['must'] as $key => $value){
                if(is_array($value)){
                    foreach ($value as $newvalue){
                        $params['body']['query']['bool']['must'][] = ['match_phrase' => [$key => $newvalue]];
                    }
                }else {
                    $params['body']['query']['bool']['must'][] = ['match_phrase' => [$key => $value]];
                }
            }

            if(isset($term['must_not'])) {
                foreach ($term['must_not'] as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $newvalue) {
                            $params['body']['query']['bool']['must_not'][] = ['match' => [$key => $newvalue]];
                        }
                    } else {
                        $params['body']['query']['bool']['must_not'][] = ['match' => [$key => $value]];
                    }
                }
            }

            if ($sortby != null) {
                $params['body']['sort'] = [$sortby => ['order' => $sort]];
            }


            return $this->client->search($params);
        }catch (Missing404Exception $e){

        }
    }

    public function getAll($term = null, $from = 0, $size = 100){
        if($term == null){
            $term = new \stdClass();
        }
        $params = [
            "from" => $from,
            "size" => $size,
            "index" => $this->index,
            "body" => [
                "query" => [
                    "match" => $term
                ]
            ]
        ];

        return $this->client->search($params);
    }

    public function deleteIndex(){
        $params = ['index' => $this->index];
        return $this->client->indices()->delete($params);
    }

}