<?php


namespace service\Core;


class View
{
    public function render($path, $params = []){
        foreach ($params as $key => $param){
            $this->$key = $param;
        }
        include VDIR.'/'.$path.'.php';
    }


    public function renderJSON($array){
        header('Content-Type: application/json');
        echo json_encode($array);
    }
}