<?php

namespace App\Controller;

use App\Utility\Database;
use App\Utility\Input;
use service\Core\Controller;
use service\Utility\Elasticsearch;

class Index extends Controller
{
    public $elastic;

    public function __construct()
    {
        parent::__construct();
        $this->elastic = new Elasticsearch();
    }

    public function index($req, $res, $args)
    {
        $this->View->render('Index/index');
    }

    public function search($req, $res, $args)
    {

        $term = Input::post('term');

        $notArray = [];

        $this->elastic->index = 'favorites';
        $favoriteFilter = $this->elastic->filter(['must'=>['term' => $term]], 'order', 'desc');


        $searchArray = [];
        $filtercat = [];
        $filterArray['must'] = ['name' => $term];

        if(count($favoriteFilter['hits']['hits']) > 0) {
            foreach ($favoriteFilter['hits']['hits'] as $forCategory) {
                $filterArray['must']['category'] = [];
                foreach ($forCategory['_source']['source']['category'] as $item) {
                    $filterArray['must']['category'][] = $item;
                }
                $filtercat[] = $item;

                $this->elastic->index = 'products';
                $filter = $this->elastic->filter($filterArray);
                $searchArray = array_merge($searchArray, $filter['hits']['hits']);
            }
        }

        $this->elastic->index = 'products';
        $filterArray['must_not']['category'] = $filtercat;
        $filter = $this->elastic->filter(['must'=>['name' => $term], 'must_not'=>$filterArray['must_not']], null, null);
        $searchArray = array_merge($searchArray, $filter['hits']['hits']);


        $this->View->render('Index/search', ['last_search'=>$favoriteFilter['hits']['hits'],'filter' => $searchArray, 'term' => $term]);
    }

    public function detail()
    {
        $id = Input::get('id');
        $term = Input::get('term');
        $detail = $this->elastic->get($id);


        $this->elastic->index = 'favorites';
        $detailByProductId = $this->elastic->filter(['must'=>['product_id' => $detail['_id']]]);
        if ($detailByProductId == null || count($detailByProductId['hits']['hits']) == 0) {
            $this->elastic->index(['term' => $term, 'order' => 1, 'product_id' => $detail['_id'], 'source' => $detail['_source']]);
        } else {
            $newBody = ['term' => $term, 'order' => $detailByProductId['hits']['hits'][0]['_source']['order'] + 1, 'product_id' => $detail['_id'], 'source' => $detail['_source']];
            $this->elastic->update($detailByProductId['hits']['hits'][0]['_id'], $newBody);
        }

        $this->View->render('Index/detail', ['detail' => $detail['_source'], 'term' => $term]);
    }

}